package com.example.training5.ipc1;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Third extends AppCompatActivity {
    EditText editName, editVal1, editVal2;
    TextView resultView;
    protected Myaidl calService = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
    }
    @Override
    protected void onStart() {
        super.onStart();

        editVal1 = (EditText) findViewById(R.id.editText4);
        editVal2 = (EditText) findViewById(R.id.editText5);

        if (calService == null) {
            Intent it = new Intent("multiplyservice");
            bindService(it, connection, Context.BIND_AUTO_CREATE);
        }
    }
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            calService =  Myaidl.Stub.asInterface(service);
            Toast.makeText(getApplicationContext(),	"Service Connected", Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            calService = null;
            Toast.makeText(getApplicationContext(), "Service Disconnected", Toast.LENGTH_SHORT).show();
        }
    };
    void multiply(View view)
    {
        int num1 = Integer.parseInt(editVal1.getText().toString());
        int num2 = Integer.parseInt(editVal2.getText().toString());
        try {
            int result = calService.getResult(num1, num2);
            String msg = calService.getMessage(editName.getText().toString());
            Toast.makeText(this,"Message:"+msg+"Result"+result,Toast.LENGTH_LONG).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
