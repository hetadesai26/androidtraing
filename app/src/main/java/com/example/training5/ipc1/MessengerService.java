package com.example.training5.ipc1;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class MessengerService extends Service {
    public MessengerService() {
    }
    public static final int TO_UPPERCASE=1;
    public static final int TO_UPPER_CASE_RESPONSE =1 ;
    private Messenger messenger =new Messenger(new ConvertHandler());
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }
    class ConvertHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int msgType = msg.what;
            switch (msgType) {
                case TO_UPPERCASE:
                    String data = msg.getData().getString("data");//getting the message by using key
                    Message resp = Message.obtain(null, TO_UPPER_CASE_RESPONSE);//Creating message object for response

                    Bundle bResp = new Bundle();//need bundle
                    bResp.putString("respData", data.toUpperCase());//put the data into the bundle

                    resp.setData(bResp);//attaching the bunle on the reply message
                    try {
                        msg.replyTo.send(resp);//replying the message with those data
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    super.handleMessage(msg);

            }
        }
    }
}
