package com.example.training5.ipc1;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Random;

public class MyService extends Service {
    public MyService() {
    }

    private final IBinder Binder = new LocalBinder();

    private final Random Generator = new Random();


    public class LocalBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return Binder;
    }
    public int getSum(int a,int b) {
        int sum=a+b;
        return sum;

    }
}
