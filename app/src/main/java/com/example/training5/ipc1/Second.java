package com.example.training5.ipc1;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Second extends AppCompatActivity {
    private ServiceConnection sConn;
    private Messenger messenger;
    EditText etText;
    TextView tvText;
    boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        etText = (EditText) findViewById(R.id.editText3);
        tvText = (TextView) findViewById(R.id.textView3);
        Button btn = (Button) findViewById(R.id.button);
    }

    void convert(View view) {
        String val = etText.getText().toString();

        Message msg = Message.obtain(null, MessengerService.TO_UPPERCASE);//who will get the message…
        msg.replyTo = new Messenger(new ResponseHandler());//it defines that where the response will go when we get it..
        Bundle b = new Bundle();
        b.putString("data", val);
        msg.setData(b);//user data (usa in our case), what we are sending to convert
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
    private ServiceConnection Connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            messenger = new Messenger(service);//this messenger is the reference of the service     messenger
            mBound=true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    class ResponseHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int respCode = msg.what;

            switch (respCode) {
                case MessengerService.TO_UPPER_CASE_RESPONSE: {
                    String result = msg.getData().getString("respData");
                    tvText.setText(result);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        Intent in=new Intent(this, MessengerService.class);
        bindService(in,Connection,Context.BIND_AUTO_CREATE);
        super.onStart();
    }
}
