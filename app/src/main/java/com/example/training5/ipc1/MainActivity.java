package com.example.training5.ipc1;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    MyService Service;
    boolean Bound = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, Connection, Context.BIND_AUTO_CREATE);
    }
    private ServiceConnection Connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyService.LocalBinder binder = (MyService.LocalBinder) service;
            Service = binder.getService();
            Bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Bound = false;
        }
    };
    void click(View view)
    {
        if (Bound) {
            EditText a1=(EditText)findViewById(R.id.editText);
            EditText b1=(EditText)findViewById(R.id.editText2);
            int a=Integer.parseInt(a1.getText().toString());
            int b=Integer.parseInt(b1.getText().toString());
            int num = Service.getSum(a,b);
            Toast.makeText(this, "number: " + num, Toast.LENGTH_SHORT).show();
        }
    }
    void aidl(View view)
    {
        Intent in=new Intent(this,Third.class);
        startActivity(in);
    }
    @Override
    protected void onStop() {
        super.onStop();
        unbindService(Connection);
        Bound = false;
    }
    void second(View view)
    {
        Intent in=new Intent(this,Second.class);
        startActivity(in);
    }

}
