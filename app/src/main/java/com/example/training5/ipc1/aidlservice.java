package com.example.training5.ipc1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

public class aidlservice extends Service {
    private final Myaidl.Stub mBinder = new Myaidl.Stub() {
        @Override
        public String getMessage(String name) throws RemoteException {
            return "Hello";
        }

        @Override
        public int getPid() throws RemoteException {
            return 0;
        }

        @Override
        public int getResult(int val1, int val2) throws RemoteException {
            return val1*val2;
        }

        public void basicTypes(int anInt, long aLong, boolean aBoolean,
                               float aFloat, double aDouble, String aString) {
            // Does nothing
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
