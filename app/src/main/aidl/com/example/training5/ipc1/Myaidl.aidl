// Myaidl.aidl
package com.example.training5.ipc1;

// Declare any non-default types here with import statements

interface Myaidl {
    String getMessage(String name);
    int getPid();
    int getResult(int val1, int val2);

}
